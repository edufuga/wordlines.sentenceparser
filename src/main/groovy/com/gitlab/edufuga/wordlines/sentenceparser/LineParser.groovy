package com.gitlab.edufuga.wordlines.sentenceparser

import groovy.transform.CompileStatic

@CompileStatic
class LineParser
{
    private static sentence = ~/»?‘?'?.*?[\.\?\!]”?’?'?«?/

    static List<String> parseLineIntoSentences(String line) {
        List<String> sentences = new ArrayList<>()
        def matches = line =~ sentence
        if (!matches) {
            sentences.add(line)
        }
        else {
            matches.each { String it -> sentences.add(it) } // without .trim()
        }

        // Check: The sum of the sentences is equal to the line when the splitting with regexes worked. If not, then
        // as a fallback we return the whole line. This way there is no loss of information.
        // NOTE: I guess this is quite expensive. Commenting this out for now. If I see strange behaviour, put it back...
        if (String.join("", sentences) != line) {
            // println "Sentences not equal to line: $sentences VS $line"
            return Arrays.asList(line)
        }

        return sentences
    }

    static void main(String[] args) {
        for (String arg : args) {
            for (String line : parseLineIntoSentences(arg)) {
                println line
            }
        }
    }
}
