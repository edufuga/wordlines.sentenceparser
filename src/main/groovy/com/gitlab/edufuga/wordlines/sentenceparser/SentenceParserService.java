package com.gitlab.edufuga.wordlines.sentenceparser;

import com.gitlab.edufuga.wordlines.core.Status;
import com.gitlab.edufuga.wordlines.core.WordStatusDate;
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader;
import com.gitlab.edufuga.wordlines.recordreader.RecordReader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class SentenceParserService {
    private final Path statusRoot;
    private final Map<String, RecordReader> readersByLanguage;
    private final Map<String, SentenceParser> sentenceParsersByLanguage;

    public SentenceParserService() {
        this.readersByLanguage = new HashMap<>();
        this.sentenceParsersByLanguage = new HashMap<>();

        String vocabularyRootAsString = System.getProperty("bookwards.vocabulary");
        if (vocabularyRootAsString == null || vocabularyRootAsString.isEmpty()) {
            Map<String, String> env = System.getenv();
            if (!env.containsKey("WORDSFILES")) {
                throw new RuntimeException("Neither the system property 'bookwards.vocabulary' " +
                        "nor the environment variable 'WORDSFILES' was set! You have to tell me where your vocabulary " +
                        "is found.");
            }
            vocabularyRootAsString = env.get("WORDSFILES");
        }
        System.out.println("Vocabulary root: " + vocabularyRootAsString);

        this.statusRoot = Paths.get(vocabularyRootAsString).resolve("status");
        System.out.println(statusRoot.toAbsolutePath());
        if (Files.notExists(statusRoot)) {
            throw new RuntimeException("Words files root not found");
        }
    }

    public List<WordStatusDate> parseSentence(String language, String sentence) {
        try {
            SentenceParser sentenceParser = getSentenceParserFor(language);
            List<WordStatusDate> parsedSentence = sentenceParser.parseSentenceIntoRecords(sentence);
            return parsedSentence.stream().map(r -> {
                        if (r.getStatus() == null) {
                            return new WordStatusDate(r.getWord(), Status.NEW, new Date());
                        }
                        else {
                            return r;
                        }
                    }
            ).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private SentenceParser getSentenceParserFor(String language) throws Exception {
        sentenceParsersByLanguage.putIfAbsent(language, new SentenceParser(getRecordReaderFor(language)));
        return sentenceParsersByLanguage.get(language);
    }

    private RecordReader getRecordReaderFor(String language) throws Exception {
        Path statusFolder = statusRoot.resolve(language);
        readersByLanguage.putIfAbsent(language, new FileSystemRecordReader(statusFolder.toAbsolutePath().toString(), null));
        return readersByLanguage.get(language);
    }

    public static void main(String[] args) {
        System.out.println("The SentenceParserService is meant to be used as a dependency.");
    }
}
