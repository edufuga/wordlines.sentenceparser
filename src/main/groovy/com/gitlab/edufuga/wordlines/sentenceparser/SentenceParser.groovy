package com.gitlab.edufuga.wordlines.sentenceparser

import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader
import groovy.transform.CompileStatic

import com.gitlab.edufuga.wordlines.core.WordStatusDate
import com.gitlab.edufuga.wordlines.recordreader.RecordReader

import java.nio.file.Files
import java.nio.file.Paths
import java.text.ParseException

@CompileStatic
class SentenceParser
{
    private RecordReader recordReader

    SentenceParser(RecordReader recordReader) {
        this.recordReader = recordReader
    }

    List<WordStatusDate> parseSentenceIntoRecords(String sentence) {
        // Split sentence into words.
        List<String> words = new ArrayList<>()
        def w = sentence =~ /[^-\.,:;…¡!¿?—\/–'"‘“”„’«»‹›()\[\]{}|_<>\\\^`+%*=~$&©@#⇒⇔Ã¼¾\d\s]*/
        w.grep().each { words << (it as String)}

        // Read records from the list of words
        // N.B: Words without a record will be ignored. The list only contains known words (i.e. that have a record)

        try {
            return readRecords (words)
        }
        catch (Exception ignored)
        {
            return Collections.emptyList()
        }
    }

    private List<WordStatusDate> readRecords(List<String> words) throws IOException, ParseException {
        // Read records from the list of words
        // N.B: Words without a record will be ignored. The list only contains known words (i.e. that have a record)
        List<WordStatusDate> records = new ArrayList<>();
        for (String word : words) {
            WordStatusDate record = recordReader.readRecord(word);
            //WordStatusDate recordWithOriginalCase = new WordStatusDate(word, record.getStatus(), record.getDate());
            records.add(record);
        }

        return records
    }

    static void main(String[] args) {
        if (args.size() < 1) {
            return;
        }

        String statusFolder = args[0]

        if (Files.exists(Paths.get(statusFolder))) {
            RecordReader recordReader = new FileSystemRecordReader(statusFolder)
            SentenceParser sentenceParser = new SentenceParser(recordReader)

            for (String line : args.tail()) {
                List<WordStatusDate> splittedSentence = sentenceParser.parseSentenceIntoRecords(line)
                println splittedSentence.join("\n")
            }
        }
        else {
            LineParser.main(args)
        }
    }
}
